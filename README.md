docker-4-simple-web-server

BUILD IMAGE
docker build -t simple-web-server .

LIST IMAGES
docker images



RUN CONTAINER
docker run -p 49160:8080 -d simple-web-server

LIST RUNNING CONTAINERS
docker ps

CHECK CONTAINER LOGS
docker logs <container id>

LIVE CONTAINER LOGS
docker attach <container id>



ACCESS TO THE CONTAINER
docker exec -it <container id> /bin/bash



Reference: https://nodejs.org/en/docs/guides/nodejs-docker-webapp/