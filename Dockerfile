FROM node:latest

# Create app directory
WORKDIR /usr/src/app

#Update the machine
RUN apt-get update

#Clone the repo
RUN git clone git@gitlab.com:jmir.17/simple-web-server.git

# Install app dependencies
RUN npm install

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "npm", "start" ]

